



$('.goods-slider-top__cards').slick({
  centerMode: true,
  centerPadding: '40px',
  slidesToShow: 3,
  arrows: true,
  autoplay: false,
  autoplaySpeed: 3000,
  prevArrow: ('<div class="arrow_left"></div>'),
  nextArrow: ('<div class="arrow_right"></div>'),
  responsive: [
    {
      breakpoint: 1352,
      settings: {
   
        centerMode: true,
        centerPadding: '90px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 990,
      settings: {
       
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
       
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
       
        slidesToShow: 1
      }
    }
  ]
});
$('.goods-slider-bottom__cards').slick({
  centerMode: true,
  centerPadding: '40px',
  slidesToShow: 3,
  arrows: true,
  autoplay: false,
  autoplaySpeed: 1500,
  prevArrow: ('<div class="arrow_left"></div>'),
  nextArrow: ('<div class="arrow_right"></div>'),
  responsive: [
    {
      breakpoint: 1352,
      settings: {
        
        centerMode: true,
        centerPadding: '90px',
        slidesToShow: 2
      }
    },
    {
      breakpoint: 990,
      settings: {
        
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        
        slidesToShow: 1
      }
    }
  ]
});
$('form').submit(function(event) {
  event.preventDefault();
  $.ajax({
    type: "POST",
    url: "mailer/smart.php",
    data: $(this).serialize()
  }).done(function() {
    $(this).find("input").val("");
    alert("Спасибо, что выбрали нас! Ваша завяка будет рассмотрена в ближайшее время!");
    $("form").trigger("reset");
  });
  return false;
});


$(function(){
  //2. Получить элемент, к которому необходимо добавить маску
  $("#input_tel-mask").mask("+7(999) 999-9999");
});

$("input").on("keypress", function(e) {
  
  var char = /["а-яА-Я]/;
  var val = String.fromCharCode(e.which);
  var test = char.test(val);
  
  if(!test) return false
});

$(function() {
  var navLink = $('.header__nav-item-line, .footer__nav-item-line');

  navLink.on('click', function(event){
    event.preventDefault();
    var target = $(this).attr('href');
    var top = $(target).offset().top;
    $('html,body').animate({scrollTop: top}, 600)
  });
});

// $(document).ready(function(){
// 	/**
// 	 * При прокрутке страницы, показываем или срываем кнопку
// 	 */
// 	$(window).scroll(function () {
// 		// Если отступ сверху больше 500px то показываем кнопку "Наверх"
// 		if ($(this).scrollTop() > 500) {
// 			$('#back-top').fadeIn();
// 		} else {
// 			$('#back-top').fadeOut();
// 		}
// 	});
	
// 	/** При нажатии на кнопку мы перемещаемся к началу страницы */
// 	$('#back-top').click(function () {
// 		$('body,html').animate({
// 			scrollTop: 0
// 		}, 500);
// 		return false;
// 	});
	
// });
$(window).scroll(function() {
  if ($(this).scrollTop() > 100) {
      if ($('.back-button').is(':hidden')) {
          $('.back-button').css({opacity : 1}).fadeIn('slow');
      }
  } else { $('.back-button').stop(true, false).fadeOut('fast'); }
});
$('.back-button').click(function() {
  $('html, body').stop().animate({scrollTop : 0}, 300);
});